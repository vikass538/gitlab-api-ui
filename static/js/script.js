// Function to fetch feeds based on selected process
function getFeeds() {
    var process = document.getElementById('selectProcess').value;
    fetch('/get-feeds/' + process)
        .then(response => response.json())
        .then(data => {
            var selectFeeds = $('#selectFeeds');
            selectFeeds.empty();
            data.feeds.forEach(feed => {
                selectFeeds.append('<option value="' + feed + '">' + feed + '</option>');
            });
            selectFeeds.prop('disabled', false);
            selectFeeds.select2(); // Initialize Select2
            document.getElementById('feedsGroup').style.display = 'block'; // Display feed section
            adjustFeedInputWidth(); // Adjust the width of the feed input box
        })
        .catch(error => console.error('Error:', error));
}

// Function to adjust the width of the feed input box
function adjustFeedInputWidth() {
    var selectProcessWidth = $('#selectProcess').outerWidth();
    $('#selectFeeds').css('width', selectProcessWidth); // Set the width of selectFeeds to match selectProcess
}

// Function to process the form
function processForm() {
    // Your form processing logic here
}

// Add event listener for selectProcess
document.getElementById('selectProcess').addEventListener('change', getFeeds);

// Adjust feed input box width when DOM content is loaded
document.addEventListener('DOMContentLoaded', adjustFeedInputWidth);

