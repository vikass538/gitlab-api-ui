import requests

class GitLabAPI:
    def __init__(self, base_url, token):
        self.base_url = base_url.rstrip('/')
        self.token = token
        self.session = requests.Session()
        self.session.headers.update({'PRIVATE-TOKEN': self.token})

    def trigger_pipeline(self, project_id, ref, variables=None):
        endpoint = f"{self.base_url}/projects/{project_id}/trigger/pipeline"
        data = {'ref': ref}
        if variables:
            data['variables[VAR_NAME]'] = variables
        response = self.session.post(endpoint, data=data)
        return response.json()

    def get_pipeline_status(self, project_id, pipeline_id):
        endpoint = f"{self.base_url}/projects/{project_id}/pipelines/{pipeline_id}"
        response = self.session.get(endpoint)
        return response.json()

    def get_last_10_pipelines(self, project_id, user=None):
        endpoint = f"{self.base_url}/projects/{project_id}/pipelines"
        params = {'per_page': 10}

        if user and user != "All":
            params['username'] = user

        response = self.session.get(endpoint, params=params)
        return response.json()

