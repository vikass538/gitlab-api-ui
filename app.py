from flask import Flask, render_template, request, jsonify
from scripts.gitlab_api import GitLabAPI
import json


app = Flask(__name__)

with open('config/config.json', 'r') as f:
    # Load the JSON data
    params = json.load(f)

gitlab = GitLabAPI(params["gitlab-url"], params["private-toke"])

json_data = {
    "processes": ["CAL", "OS"],
    "feeds": {
        "CAL": [
            "GLB11",
            "GLBT12",
            "GLBT13",
            "GLBT14",
            "GLBT15"
        ],
        "OS": [
            "MUR11",
            "MUR12",
            "MUR13",
            "MUR14",
            "MUR15"
        ]
    }
}

@app.route('/')
def index():
    return render_template('index.html')

# @app.route('/trigger-pipeline')
# def trigger_pipeline():
#     response = gitlab.trigger_pipeline(params["project_id"], params["gitlab-branch"], "variables")
#     return render_template('trigger_pipeline.html', processes=params["process"].keys())
#     return render_template('trigger_pipeline.html')

@app.route('/trigger-pipeline')
def trigger_pipeline():
    return render_template('trigger_pipeline.html', processes=json_data["processes"])


@app.route('/get-feeds/<process>', methods=['GET', 'POST'])
def get_feeds(process):
    feeds = json_data["feeds"].get(process, [])
    return jsonify(feeds=feeds)
    # if request.method == 'POST':
    #     process = request.form.get('selectProcess')
    #     feeds = request.form.getlist('selectFeeds')
    #     pipeline_variable = f'{process}={feeds}'
    #     status = gitlab.trigger_pipeline(params["project_id"], params["gitlab-branch"], pipeline_variable)
    #     return render_template('trigger_pipeline.html', status=status)


@app.route('/pipeline-status', methods=['GET', 'POST'])
def pipeline_status():
    if request.method == 'POST':
        pipeline_id = request.form['pipelineId']
        status = gitlab.get_pipeline_status(params["project_id"], pipeline_id)
        return render_template('pipeline_status.html', pipeline_status=status)
    else:
        return render_template('pipeline_status.html')


@app.route('/execution-history', methods=['GET', 'POST'])
def execution_history():
    if request.method == 'POST':
        user = request.form['user']
        last_10_pipelines = gitlab.get_last_10_pipelines(params["project_id"], user)
        return render_template('execution_history.html', pipelines=last_10_pipelines)
    else:
        return render_template('execution_history.html')

# Run the app
if __name__ == '__main__':
    app.run(debug=True)
